# use the same OS for compiling crunchers eclipse-temurin:21-jre is built upon
# https://github.com/adoptium/containers/blob/main/21/jre/ubuntu/noble/Dockerfile
FROM ubuntu:24.04 AS crunchers

RUN set -xe \
	&& apt-get update \
	&& apt-get install -y unzip build-essential

# https://bitbucket.org/magli143/exomizer/wiki/Home
ARG EXOMIZER=exomizer-3.1.2.zip
WORKDIR /usr/src/exomizer

COPY "${EXOMIZER}" .

RUN set -xe \
	&& unzip "${EXOMIZER}" \
	&& cd src/ \
	&& make \
	&& cp -a exomizer exobasic ../ \
	&& cd ../ \
	&& ./exomizer -v \
	&& ./exobasic -v

# https://csdb.dk/search/?seinsel=releases&search=Dali
ARG DALI=dali-ce5a563.tar.gz
WORKDIR /usr/src/dali

COPY "${DALI}" .

RUN set -xe \
	&& tar -xzf "${DALI}" --strip-components=1 \
	&& make \
	&& ./dali --version


FROM eclipse-temurin:21-jre

RUN set -xe \
	&& mkdir -p /usr/src/kickass \
	&& apt-get update \
	&& apt-get install -y unzip

# http://www.theweb.dk/KickAssembler/Main.html
# https://github.com/p-a/kickass-cruncher-plugins/releases
COPY KickAssembler.zip kickass-cruncher-plugins-2.1.zip /usr/src/kickass/
RUN set -xe \
	&& cd /usr/src/kickass \
	&& unzip KickAssembler.zip KickAss.jar \
	&& rm KickAssembler.zip \
	&& unzip -j kickass-cruncher-plugins-2.1.zip kickass-cruncher-plugins-2.1/kickass-cruncher-plugins-2.1.jar \
	&& rm kickass-cruncher-plugins-2.1.zip \
	&& ln -s /src/KickAss.cfg . \
	&& mkdir -p /src/

RUN set -xe \
	&& apt-get remove -y unzip \
	&& apt-get autoremove -y --purge \
	&& apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV CLASSPATH=/usr/src/kickass/*

WORKDIR /src/

COPY --from=crunchers /usr/src/exomizer/exomizer /usr/src/exomizer/exobasic /usr/src/dali/dali /usr/bin/

RUN set -xe \
	&& java --version \
	&& java kickass.KickAssembler /dev/null -o /tmp/dummy.prg \
	&& rm /tmp/dummy.prg \
	&& exomizer -v \
	&& exobasic -v \
	&& dali --version
