# Kick Assembler Docker image ready for compiling

## Image contents
Based on Eclipse Temurin™ 21 JRE.

Contains latest:
- Kick Assembler (http://www.theweb.dk/KickAssembler/)
- KickAss Cruncher Plugins (https://github.com/p-a/kickass-cruncher-plugins)
- Exomizer (https://bitbucket.org/magli143/exomizer/wiki/Home)
- Dali (https://github.com/bboxy/bitfire/tree/master/packer/dali)

The container's CLASSPATH variable includes the corresponding .jar files already.

## Usage
To compile your project, mount your project directory as `/src/` into the container and execute Kick Assembler with any parameters you desire.
```
docker run --rm --volume /your/project/dir:/src/ registry.gitlab.com/robotriot/kickass java kickass.KickAssembler your-source-file.asm
```

E.g. to start compilation of `main.asm` from inside your project directory, use:
```
docker run --rm --volume "$(pwd)":/src/ registry.gitlab.com/robotriot/kickass java kickass.KickAssembler main.asm
```

## Configuration
The container's Kick Assembler does NOT have any default `KickAss.cfg` in place, instead it looks for it in the (mounted) `/src/` directory. This allows you to simply put a `KickAss.cfg` file into your project's root directory and have KickAssembler pick that up when you mount it into the container. This feature is optional so you don't have to put a config file there.

FYI, the default `KickAss.cfg` contains the following lines:
```
-showmem
-symbolfile
```

